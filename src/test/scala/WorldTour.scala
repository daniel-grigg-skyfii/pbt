import org.scalacheck.Prop.{BooleanOperators, forAll}
import org.scalacheck._
import Arbitrary.arbitrary
import org.scalacheck.Prop._

object WorldTour extends Properties("WorldTour") {

  // Use a Generator to restrict test values to (0,100)
  val smallInteger = Gen.choose(0, 100)
  property("smallIntsAreSmall") = Prop.forAll(smallInteger) { n => n >= 0 && n <= 100 }

  // Implication - given n in (0,1000) ==> list.fill length is n
  property("implications") = Prop.forAll { n: Int =>
    (n >= 0 && n < 1000) ==> (List.fill(n)("").length == n)
  }

  // Logical combiners
  val p1 = forAll { n: Int => n * 2 == n + n }
  val p2 = forAll { n: Int => n + 0 == n }
  property("logical combiners") = p1 && p2


  def myMagicFunction(n: Int, m: Int) = n + m

  property("labels") = Prop.forAll { (m: Int, n: Int) =>
    val res = myMagicFunction(n, m)
    (res >= m) :| "result > #1" &&
      (res >= n) :| "result > #2" &&
      (res < m + n) :| "result not sum"
  }

  // Some generators
  val g1 = for { n <- Gen.choose(1,100); m <- Gen.oneOf('A', 'E', 'I', 'O', 'U') } yield (m,n)

  // Generating case classes
  case class Person(firstName: String, lastName:String, age:Int, alive:Boolean)

  val genLastNames = Gen.frequency(
    (42, "Smith"),
    (21, "Jones"),
    (20, "Williams"),
    (20, "Brown"),
    (17, "Wilson")
  )

  val genFirstNames = Gen.oneOf("Isabella", "Olivia", "Chloe", "William", "Jack", "Oliver")

  val genPerson = for {
    name <- genFirstNames
    last <- genLastNames
    age <- Gen.choose(1, 101)
    alive <- arbitrary[Boolean]
  } yield Person(name, last, age, alive)

  // Generating lists
  val genCommunity = Gen.containerOf[List, Person](genPerson)
  val genCommunity5 = Gen.containerOfN[List, Person](4, genPerson)

  implicit lazy val arbPerson: Arbitrary[Person] = Arbitrary { genPerson }

  property("living and old") = forAll { x:Person =>
    classify(x.alive, "living", "dead") {
      classify(x.age > 80, "old") {
        x.age > 0

      }
    }
  }
}
